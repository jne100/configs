#! /bin/bash


UTILSPATH="`dirname $0`"/utils
UTILDIRNAME=".jne100_utils"


xinstall () {
    local name=$1 output
    local red='\033[0;31m' green='\033[0;32m' nc='\033[0m'
    output=`install_util $name`
    if [ $? = 0 ]; then
        echo -e "Install $name util: ${green}OK${nc}"
    else
        echo -e "Install $name util: ${red}${output}${nc}"
    fi
}


install_util () {
    local name=$1
    if [ ! -d "$UTILSPATH/$name" ]; then
        echo -n "$name util is missing"
        return 1
    fi
    rm -rf ~/$UTILDIRNAME/$name
    mkdir -p ~/$UTILDIRNAME
    cp -R $UTILSPATH/$name ~/$UTILDIRNAME/
    return 0
}


xinstall foobard
xinstall httpsrv
xinstall hwbash
xinstall hwc
xinstall hwcpp
xinstall hwgo
xinstall hwpl
xinstall hwpy
