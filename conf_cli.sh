#! /bin/bash


DATAPATH="`dirname $0`"/data


configure () {
    local name=$1 output status
    local red='\033[0;31m' green='\033[0;32m' nc='\033[0m'
    output=`conf_$name`
    status=$?
    if [ $status = 0 ]; then
        echo -e "Configure $name: ${green}OK${nc}"
    else
        echo -e "Configure $name: ${red}${output}${nc}"
    fi
    return $status
}


conf_home () {
    if [ ! -d "$HOME/bin" ] ; then
        mkdir "$HOME/bin"
    fi
    if [ ! -d "$HOME/lib/go" ] ; then
        mkdir -p "$HOME/lib/go"
    fi
    return 0
}


conf_bash () {

    local subdir=".jne100_configs"
    local profile=".fucking_single_bash_profile"
    if [ ! -f "$DATAPATH/$profile" ]; then
        echo -n "$profile is missing"
        return 1
    fi
    mkdir -p ~/$subdir
    command rm -f ~/$subdir/*
    cp "$DATAPATH/$profile" ~/$subdir/

    # patch .bashrc
    [ -f ~/.bashrc ] || touch ~/.bashrc
    if ! grep -q "$profile" ~/.bashrc; then
        echo -e "\n\n[ -f ~/$subdir/$profile ] && . ~/$subdir/$profile\n\n" \
            >> ~/.bashrc
    fi

    # patch .bash_profile
    [ -f ~/.bash_profile ] || touch ~/.bash_profile
    if ! grep -q "$profile" ~/.bash_profile; then
        echo -e "\n\n[ -f ~/$subdir/$profile ] && . ~/$subdir/$profile\n\n" \
            >> ~/.bash_profile
    fi

    return 0
}


conf_zsh () {

    local subdir=".jne100_configs"
    local profile=".fucking_single_zsh_profile"
    if [ ! -f "$DATAPATH/$profile" ]; then
        echo -n "$profile is missing"
        return 1
    fi
    mkdir -p ~/$subdir
    command rm -f ~/$subdir/*
    cp "$DATAPATH/$profile" ~/$subdir/

    # patch .zshrc
    [ -f ~/.zshrc ] || touch ~/.zshrc
    if ! grep -q "$profile" ~/.zshrc; then
        echo -e "\n\n[ -f ~/$subdir/$profile ] && . ~/$subdir/$profile\n\n" \
            >> ~/.zshrc
    fi

    # patch .zprofile
    [ -f ~/.zprofile ] || touch ~/.zprofile
    if ! grep -q "$profile" ~/.zprofile; then
        echo -e "\n\n[ -f ~/$subdir/$profile ] && . ~/$subdir/$profile\n\n" \
            >> ~/.zprofile
    fi

    return 0
}


conf_git () {
    if ! which git &> /dev/null; then
        echo -n "git is not installed"
        return 1
    fi
    git config --global color.ui true
    git config --global core.editor vim
    git config --global user.name Nikita
    git config --global user.email jne100@gmail.com
    return 0
}


conf_gdb () {
    if [ ! -f "$DATAPATH/.gdbinit" ]; then
        echo -n ".gdbinit is missing"
        return 1
    fi
    cp "$DATAPATH/.gdbinit" ~/
    [ -f "$DATAPATH/.gdbinit.local.sample" ] && \
        cp "$DATAPATH/.gdbinit.local.sample" ~/
    return 0
}


conf_vim () {
    if [ ! -f "$DATAPATH/.vimrc" ]; then
        echo -n ".vimrc is missing"
        return 1
    fi
    cp "$DATAPATH/.vimrc" ~/
    return 0
}


#configure home
configure bash
configure zsh
configure git
#configure gdb
configure vim
