#! /bin/bash

# swap left ctrl and fn
# this have to be swapped manually in bios (F1)

# swap home and pgup buttons, end and pgdown buttons
xmodmap -e "keycode 110 = Prior"
xmodmap -e "keycode 112 = Home"
xmodmap -e "keycode 117 = End"
xmodmap -e "keycode 115 = Next"

xmodmap -pke > ~/.Xmodmap
echo 'xmodmap .Xmodmap' > ~/.xinitrc

DATAPATH="`dirname $0`"/data
cp "$DATAPATH/run_xinitrc.desktop" ~/.config/autostart/
