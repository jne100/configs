#! /bin/bash

NAME='Sublime Text'

# check root access
if [ "$(id -u)" != "0" ]; then
    echo Warning: root priviledge required to install $NAME
    exit 1
fi

# create links
mkdir -p /usr/local/bin
ln -s "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" /usr/local/bin/sublime
ln -s "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" /usr/local/bin/sub
