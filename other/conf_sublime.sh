#! /bin/bash

NAME='Sublime Text'
SUBLIME_DIR=/opt/sublime
SUBLIME_CONFIG=$HOME/.config/sublime-text-2

if [ -d "$SUBLIME_DIR" ]; then
    # create shortcuts
    ln -sf $SUBLIME_DIR/sublime_text ~/bin/sublime
    ln -sf $SUBLIME_DIR/sublime_text ~/bin/sub
else
    echo Warning: $NAME is not installed
    exit 1
fi
    
if [ -d "$SUBLIME_CONFIG" ]; then

    # copy my theme
    cp data/PlainRadioactivity.tmTheme \
        $SUBLIME_CONFIG/Packages/Theme\ -\ Default/

    # copy preferences config
    cp data/Preferences.sublime-settings \
        $SUBLIME_CONFIG/Packages/User/

    # copy syntax specific preference configs
    cp data/Go.sublime-settings \
        data/Python.sublime-settings \
        $SUBLIME_CONFIG/Packages/User/

    # copy keymap config
    cp data/Default\ \(Linux\).sublime-keymap \
        $SUBLIME_CONFIG/Packages/User/

else
    echo Warning: $NAME has no config folder \(run to fix\)
    exit 1
fi

echo Status: $NAME configured
exit 0
