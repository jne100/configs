#! /bin/bash

NAME='Sublime Text'
SUBLIME_DIR='/opt/sublime'

# check root access
if [ "$(id -u)" != "0" ]; then
    echo Warning: root priviledge required to install $NAME
    exit 1
fi

# check parameters passed
if [ $# -ne "1" ]; then
    echo Warning: wrong usage of $NAME installation script
    exit 1
fi

# create sublime directory
rm -rf $SUBLIME_DIR
mkdir -p $SUBLIME_DIR

# unpack
tar vxjf "$1"
mv Sublime\ Text\ 2/* $SUBLIME_DIR
#tar vxjf $SUBLIME_DIR/"$SUBLIME_TAR"

echo Status: $NAME installed
exit 0

