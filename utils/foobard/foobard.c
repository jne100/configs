#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


int main(int argc, char* argv[])
{
    if (daemon(0, 0) == -1) {
        perror("daemon");
        return -1;
    }

    for (;;) {
        sleep(1);
    }

    return 0;
}
