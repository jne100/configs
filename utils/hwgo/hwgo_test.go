package main

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
)

func BenchmarkListToStringStd(b *testing.B) {
	l := []int64{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	for i := 0; i < b.N; i++ {
		fmt.Sprint(l)
	}
}

func BenchmarkListToStringManual(b *testing.B) {
	l := []int64{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	for i := 0; i < b.N; i++ {
		toStrManual(l)
	}
}

func toStrManual(l []int64) string {
	var builder strings.Builder
	builder.WriteRune('[')
	for i, x := range l {
		if i != 0 {
			builder.WriteRune(',')
			builder.WriteRune(' ')
		}
		builder.WriteString(strconv.FormatInt(x, 10))
	}
	builder.WriteRune(']')
	return builder.String()
}
