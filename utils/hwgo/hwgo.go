package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	fmt.Println(toStrManual([]int64{1, 2, 3}))
}

func toStrManual(l []int64) string {
	var builder strings.Builder
	builder.WriteRune('[')
	for i, x := range l {
		if i != 0 {
			builder.WriteRune(',')
			builder.WriteRune(' ')
		}
		builder.WriteString(strconv.FormatInt(x, 10))
	}
	builder.WriteRune(']')
	return builder.String()
}
