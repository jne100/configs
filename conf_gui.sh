#! /bin/bash


configure () {
    local name=$1 output status
    local red='\033[0;31m' green='\033[0;32m' nc='\033[0m'
    output=`conf_$name`
    status=$?
    if [ $status = 0 ]; then
        echo -e "Configure $name: ${green}OK${nc}"
    else
        echo -e "Configure $name: ${red}${output}${nc}"
    fi
    return $status
}


conf_gnome_aliases () {
    which gnome-terminal &> /dev/null && \
        ln -sf `which gnome-terminal` ~/bin/terminal
    which gnome-system-monitor &> /dev/null && \
        ln -sf `which gnome-system-monitor` ~/bin/monitor
    which gnome-control-center &> /dev/null && \
        ln -sf `which gnome-control-center` ~/bin/control
    return 0
}


conf_gnome_shortcuts () {
    if ! which gsettings &> /dev/null; then
        echo -n "gsettings is not installed"
        return 1
    fi
    # minimize all
    gsettings set org.gnome.desktop.wm.keybindings \
        show-desktop "['<Super>d']"
    return 0
}


conf_gnome_environment () {
    if ! which gconftool-2 &> /dev/null; then
        echo -n "gconftool-2 is not installed"
        return 1
    fi
    gconftool-2 --type boolean --set \
        /apps/gnome-terminal/profiles/Default/login_shell true
    if ! which gsettings &> /dev/null; then
        echo -n "gsettings is not installed"
        return 1
    fi
    gsettings set org.gnome.desktop.sound event-sounds false
    gsettings set org.gnome.libgnomekbd.desktop default-group '-1'
    gsettings set org.gnome.libgnomekbd.desktop group-per-window true
    return 0
}


if which gnome-session &> /dev/null; then
    configure gnome_aliases
    configure gnome_shortcuts
    configure gnome_environment
fi
