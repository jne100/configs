#! /bin/bash


xinstall () {
    local name=$1 pm=$2 status
    local red='\033[0;31m' green='\033[0;32m' nc='\033[0m'
    install_$pm_$name
    status=$?
    if [ $status = 0 ]; then
        echo -e "Install $name: ${green}OK${nc}"
    else
        echo -e "Install $name: ${red}error ($status)${nc}"
    fi
    return $status
}


install_yum () {
    local name=$1
    yum -y install $name &> /dev/null
    return $?
}


install_aptitude () {
    local name=$1
    aptitude -y install $name &> /dev/null
    return $?
}


if which yum &> /dev/null; then
    packages="git vim build-essential bash-completion xclip gdb"
    for package in $packages; do
        xinstall yum $package
    done
elif which aptitude &> /dev/null; then
    packages="git vim build-essential bash-completion xclip gdb"
    for package in $packages; do
        xinstall aptitude $package
    done
fi
