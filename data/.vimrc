"set list
"set listchars=tab:»\ ,trail:·
":set formatoptions-=r formatoptions-=c formatoptions-=o
:set hlsearch
autocmd BufNewFile,BufRead * setlocal formatoptions-=r formatoptions-=c formatoptions-=o
