alias re = refresh

focus cmd

handle SIGPIPE nostop noprint pass

# allowed local .gdbinit files list
#set auto-load safe-path /foo/bar/.gdbinit

# example of user defined command
#define updown
#up
#down
#end

